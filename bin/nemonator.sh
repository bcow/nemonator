#!/bin/bash 
# Nemonator - record audio on GNU/Linux when microphone detects noise.
# Antti Peltonen <antti.peltonen@patentpending.fi>

# configuration section

noise_threshold=3
storage_folder=~/recordings
sox_raw_options="-t raw -r 48k -e signed -b 16"
split_size=1048576 # 1M
use_sqlite=1

#
# do not touch below this line
#

raw_folder=$storage_folder/tmp
split_folder=$storage_folder/split

# init working directories
mkdir -p $raw_folder $split_folder
# ... and create fifo pipe if not present
test -a $raw_folder/in.raw || mkfifo $raw_folder/in.raw

# init sqlite database
database=""
if [ "$use_sqlite" -eq "1" ]
then
    STRUCTURE="CREATE TABLE noise (id INTEGER PRIMARY KEY, date DATETIME, value INTEGER, threshold INTEGER, recording TEXT);"
    database="$storage_folder/noise-$( date +%FT%T ).sqlite3"
    cat /dev/null > $database
    echo $STRUCTURE |sqlite3 $database
fi

# start recording and spliting in background
rec $sox_raw_options - > $raw_folder/in.raw 2>/dev/null &
split -b $split_size -a 8 - < $raw_folder/in.raw $split_folder/piece &

while true
do
    # check each finished raw file
    for raw in $( find $split_folder -size ${split_size}c )
    do 
        max_level=$( sox $sox_raw_options $raw -n stats -s 16 2>&1 |awk '/^Max\ level/ {print int($3)}' )
        now=$( date +%FT%T )
        if [ $max_level -gt $noise_threshold ]
        then
            dest="$storage_folder/recording-$now.wav"
            printf '\r'
            echo "$now $max_level/$noise_threshold => $dest"
            sox $sox_raw_options $raw $dest
            if [ "$use_sqlite" -eq "1" ]
            then
                echo "INSERT INTO noise (date, value, threshold, recording) VALUES ('$now', '$max_level', '$noise_threshold', '$( basename $dest)');" |sqlite3 $database
            fi
	else
            printf '\r'
            echo -n "$now $max_level/$noise_threshold"
            if [ "$use_sqlite" -eq "1" ]
            then
                echo "INSERT INTO noise (date, value, threshold) VALUES ('$now', '$max_level', '$noise_threshold');" |sqlite3 $database
            fi
        fi
        rm $raw
    done
    sleep 1
done

# eof
